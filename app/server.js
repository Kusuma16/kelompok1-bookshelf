const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const book = require('../routes/books/book.routes');
const customer = require('../routes/customers/customer.routes');
const category = require('../routes/category/category.routes');

const app = express();


app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/books', book);
app.use('/customers', customer);
app.use('/category', category);


module.exports = app;
