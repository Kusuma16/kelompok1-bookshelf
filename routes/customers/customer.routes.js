const express = require('express');
const router = express.Router();


const {
    getAll,
    getOne,
    create,
    destroy,
    update
} = require('../../controller/customers/customer.controller');

router.get('/', getAll);
router.get('/:id', getOne);
router.post('/', create);
router.delete('/:id', destroy);
router.put('/:id', update);

module.exports = router;