const aws = require("@aws-sdk/client-s3");
const AWS = require('aws-sdk');
const multer = require("multer");
const multers3 = require("multer-s3");
const dotenv = require('dotenv');

dotenv.config();

const s3 = new aws.S3({
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  region: "us-east-1",
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    cb(null, true);
  } else {
    cb(new Error("Invalid file type, only JPEG and PNG is allowed!"), false);
  }
};

const upload = multer({
  fileFilter,
  storage: multers3({
    acl: "public-read",
    s3,
    bucket: "kelompok1-bucket",
    key: function (req, file, cb) {
      const title = req.body.title;
      const ext = file.mimetype.split('/')[1];
      cb(null, `${title}.${ext}`);
    },
  }),
});

module.exports = {
  upload,
  AWS,
  s3
};
