const Message = {
    'SuccessfullyAdded': 'Your data has been successfully added',
    'SuccessfullyUpdated': 'Your data has been successfully updated',
    'SuccessfullyDeleted': 'Your data has been successfully deleted',
    'SuccessfullyLoaded': 'Your data has been successfully loaded',
    'InternalServerError': 'Internal server error',
    'AlreadyExist': 'Data Already Exists',
    'NotFound': 'Data Not Found',
    'InvalidData': 'Read Page cannot be more than Page Count',
}

const StatusCode = {
    OK: 200,
    Created: 201,
    InternalServerError: 500,
    NotFound: 404,
    BadRequest: 400,
    Unauthorized: 401,
    Forbidden: 403,
    Conflict: 409,
}

const bookStatus = {
    'NotRead': 'Not Read',
    'Reading': 'Reading',
    'Read': 'Read',
}

module.exports = {
    Message,
    StatusCode,
    bookStatus
}