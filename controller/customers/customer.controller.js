const Customers = require("../../models/customers/CustomerModel");
const { Message, StatusCode } = require("../../common/constant");

const getAll = async (req, res) => {
  try {
    const customers = await Customers.findAll();
    const response = {
      statusCode: StatusCode.OK,
      message: Message.SuccessfullyLoaded,
      data: customers,
    };
    res.status(StatusCode.OK).json(response);
  } catch (error) {
    return {
      status: StatusCode.InternalServerError,
      message: error.message,
    };
  }
};

const getOne = async (req, res) => {
  try {
    const id = req.params.id;
    const customer = await Customers.findOne({ where: { id: id } });
    if (customer) {
      const response = {
        statusCode: StatusCode.OK,
        message: Message.SuccessfullyLoaded,
        data: customer,
      };
      res.status(StatusCode.OK).json(response);
    } else {
      const response = {
        statusCode: StatusCode.NotFound,
        message: Message.NotFound,
      };
      res.status(StatusCode.NotFound).json(response);
    }
  } catch (error) {
    return {
      status: StatusCode.InternalServerError,
      message: error.message,
    };
  }
};

const create = async (req, res) => {
  try{
    const customer = await Customers.create(req.body);
    console.log(customer);
    const response = {
      statusCode: StatusCode.Created,
      message: Message.SuccessfullyAdded,
    };
    res.status(StatusCode.Created).json(response);
  } catch(error){
    return {
      statusCode: StatusCode.InternalServerError,
      message: error.message,
    };
  }
}

const destroy = async (req, res) => {
  try {
    const id = req.params.id;
    const customer = await Customers.findOne({ where: { id: id } });
    if (customer) {
      await Customers.destroy({ where: { id: id } });
      const response = {
        statusCode: StatusCode.OK,
        message: Message.SuccessfullyDeleted,
      };
      res.status(StatusCode.OK).json(response);
    } else {
      const response = {
        statusCode: StatusCode.NotFound,
        message: Message.NotFound,
      };
      res.status(StatusCode.NotFound).json(response);
    }
  } catch (error) {
    return {
      status: StatusCode.InternalServerError,
      message: error.message,
    };
  }
};

const update = async (req, res) => {
  try {
    const id = req.params.id;
    const customer = await Customers.findOne({ where: { id: id } });
    if (customer) {
      await Customers.update(req.body, { where: { id: id } });
      const response = {
        statusCode: StatusCode.OK,
        message: Message.SuccessfullyUpdated,
      };
      res.status(StatusCode.OK).json(response);
    } else {
      const response = {
        status: StatusCode.NotFound,
        message: Message.NotFound,
      };
      res.status(StatusCode.NotFound).json(response);
    }
  } catch (error) {
    return {
      statusCode: StatusCode.InternalServerError,
      message: error.message,
    };
  }
};

module.exports = {
  getAll,
  getOne,
  create,
  destroy,
  update,
};
