const Category = require("../../models/category/CategoryModel");
const { Message, StatusCode } = require("../../common/constant");
const { upload, s3 } = require('../../services/upload');
const singleUpload = upload.single("image");

const getAll = async (req, res) => {
  const response = await s3.listObjects({
    Bucket: 'kelompok1-bucket',
  });
  res.json(response);
};


const create = async (req, res) => {
  singleUpload(req, res, function (err, data) {
    // const categoryTitle = req.body.title;
    // console.log(categoryTitle);
    if (err) {
      return res.json({
        success: false,
        errors: {
          title: "Image Upload Error",
          detail: err.message,
          error: err,
        },
      });
    }else{
      return res.json({
        title: req.body.title,
        success: {
          message: "Your Data Has Been Uploaded"
        }
      })
    }

  });
};

module.exports = {
  getAll,
  create,
};
