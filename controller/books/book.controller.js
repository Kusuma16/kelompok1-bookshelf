const Books = require("../../models/books/BookModel");
const { Message, StatusCode } = require("../../common/constant");

const getAll = async (req, res) => {
  try {
    const books = await Books.findAll();
    const response = {
      statusCode: StatusCode.OK,
      message: Message.SuccessfullyLoaded,
      data: books,
    };
    res.status(StatusCode.OK).json(response);
  } catch (error) {
    return {
      status: StatusCode.InternalServerError,
      message: error.message,
    };
  }
};

const getOne = async (req, res) => {
  try {
    const id = req.params.id;
    const book = await Books.findOne({ where: { id: id } });
    if (book) {
      const response = {
        statusCode: StatusCode.OK,
        message: Message.SuccessfullyLoaded,
        data: book,
      };
      res.status(StatusCode.OK).json(response);
    } else {
      const response = {
        statusCode: StatusCode.NotFound,
        message: Message.NotFound,
      };
      res.status(StatusCode.NotFound).json(response);
    }
  } catch (error) {
    return {
      status: StatusCode.InternalServerError,
      message: error.message,
    };
  }
};

const create = async (req, res) => {
  try {
    const title = await Books.findOne({ where: { title: req.body.title } });
    if (title) {
      const response = {
        statusCode: StatusCode.Conflict,
        message: Message.AlreadyExist,
      };
      res.status(StatusCode.Conflict).json(response);
    } else {
      if (Books.readPage > Books.pageCount) {
        const response = {
          statusCode: StatusCode.BadRequest,
          message: Message.InvalidData,
        };
        res.status(StatusCode.BadRequest).json(response);
      }
      const book = await Books.create(req.body);

      const response = {
        statusCode: StatusCode.Created,
        message: Message.SuccessfullyAdded,
      };
      res.status(StatusCode.Created).json(response);
    }
  } catch (error) {
    return {
      statusCode: StatusCode.InternalServerError,
      message: error.message,
    };
  }
};

const destroy = async (req, res) => {
  try {
    const id = req.params.id;
    const book = await Books.findOne({ where: { id: id } });
    if (book) {
      await Books.destroy({ where: { id: id } });
      const response = {
        statusCode: StatusCode.OK,
        message: Message.SuccessfullyDeleted,
      };
      res.status(StatusCode.OK).json(response);
    } else {
      const response = {
        statusCode: StatusCode.NotFound,
        message: Message.NotFound,
      };
      res.status(StatusCode.NotFound).json(response);
    }
  } catch (error) {
    return {
      status: StatusCode.InternalServerError,
      message: error.message,
    };
  }
};

const update = async (req, res) => {
  try {
    const id = req.params.id;
    const book = await Books.findOne({ where: { id: id } });
    if (book) {
      await Books.update(req.body, { where: { id: id } });
      const response = {
        statusCode: StatusCode.OK,
        message: Message.SuccessfullyUpdated,
      };
      res.status(StatusCode.OK).json(response);
    } else {
      const response = {
        status: StatusCode.NotFound,
        message: Message.NotFound,
      };
      res.status(StatusCode.NotFound).json(response);
    }
  } catch (error) {
    return {
      statusCode: StatusCode.InternalServerError,
      message: error.message,
    };
  }
};

module.exports = {
  getAll,
  getOne,
  create,
  destroy,
  update,
};
