TAG ?= $(shell git rev-parse --abbrev-ref HEAD)
PWD=$(shell pwd)
include .env

build:
	docker build -f build/development/Dockerfile -t $(TAG) .

.PHONY: build
