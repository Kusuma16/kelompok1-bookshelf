const Sequelize = require('sequelize');
const db = require('../../initialize/db');

const {DataTypes} = Sequelize;

const Category = db.define('category', {
    title: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    image: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    
}, {
    freezeTableName: true,
});

module.exports = Category;

(async() => {
    await db.sync();
})();