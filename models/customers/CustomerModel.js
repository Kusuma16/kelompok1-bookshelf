const Sequelize = require('sequelize');
const db = require('../../initialize/db');

const {DataTypes} = Sequelize;

const Customer = db.define('customers', {
    nama: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    alamat: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    no_hp: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    
}, {
    freezeTableName: true,
});

module.exports = Customer;

(async() => {
    await db.sync();
})();