const Sequelize = require('sequelize');
const db = require('../../initialize/db');

const {DataTypes} = Sequelize;

const Book = db.define('books', {
    title: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    author: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    genre: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    pageCount: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    readPage: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    
}, {
    freezeTableName: true,
});

module.exports = Book;

(async() => {
    await db.sync();
})();